#!/usr/bin/env bash

# ==============================================================================
# Program:       podman homelab config
# Description:   config podman containers used in homelab project
# Software/Tool: podman
# ==============================================================================

INSTALL_MODE=$1
WIREGUARD_SERVER="$PWD/wireguard/config/wg0.conf"
WIREGUARD_PEER1="$PWD/wireguard/config/peer1/peer1.conf"

init_all(){
  sudo modprobe ip_tables
  sudo echo 'ip_tables' >> /etc/modules
  sudo rm -f /etc/resolv.conf

  #systemctl settings
  sudo systemctl stop firewalld
  source .env
  sudo systemctl dibable --now systemd-resolved
  sudo systemctl reload systemd-networkd
  sudo systemctl restart NetworkManager

  sudo sysctl -w net.ipv4.ip_unprivileged_port_start=53
  sudo sysctl -w net.ipv4.ip_forward=1
  sudo sysctl -w net.ipv6.conf.all.forwarding=1
  sudo sysctl -p

  podman exec $PIHOLE_CNAME apt update
  podman exec $PIHOLE_CNAME apt upgrade -y
  podman exec $PIHOLE_CNAME apt install unbound -y
  podman restart $PIHOLE_CNAME
}

init_server(){
  sudo cp $WIREGUARD_SERVER /etc/wireguard/wg0.conf
  sudo wg-quick up wg0
}

init_client(){
  sed -i "s/\(ListenPort.*$\)/#\1/g; s/\(DNS.*$\)/#\1/g" $WIREGUARD_PEER1
  echo "PersistentKeepalive = 25" >> $WIREGUARD_PEER1
  sudo cp $WIREGUARD_PEER1 /etc/wireguard/peer1.conf
  sudo wg-quick up peer1
}

case "$INSTALL_MODE" in
  init-all)
    init_all
  ;;
  init_server)
    init_server
  ;;
  init_client)
    init_client
  ;;
  *)
    echo "Unknow mode"
  ;;
esac
